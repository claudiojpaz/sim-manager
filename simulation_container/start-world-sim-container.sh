#!/bin/bash
IMAGE_NAME="ros1_world_container:0.1"
CONTAINER_NAME="world-simulation"
SIMULATION_DIR_HOST="$(pwd)/simulation"
SIMULATION_DIR_CONTAINER="/home/catkin_ws/src/"
CATKIN_DIR_CONTAINER="/home/catkin_ws/"

list_containers="$(docker ps --format '{{.Names}}' -a)"

echo "Corriendo xhost +local:docker"
xhost +local:docker #Allows GUIs for Docker

echo "Corroborando si existe un contenedor con el mismo nombre"
if ( echo "$list_containers" | fgrep -q $CONTAINER_NAME ); then
    echo " -> Existe un contenedor con el mismo nombre, se procede a eliminarlo"
    docker rm --force $CONTAINER_NAME
else
    echo " -> No existe un contenedor con el mismo nombre"
fi

echo "Procediendo a construir y ejecutar el contenedor"
docker  run -d -i \
        --net=host \
        --volume="/tmp/.X11-unix:/tmp/.X11-unix" \
        --volume="$SIMULATION_DIR_HOST:$SIMULATION_DIR_CONTAINER" \
        --name $CONTAINER_NAME \
        $IMAGE_NAME

# --env="DISPLAY=unix$DISPLAY" \

# echo "Ejecutando simulacion CTRL+C para finalizar"
docker exec -ti -w $CATKIN_DIR_CONTAINER $CONTAINER_NAME catkin build
# echo "Deteniendo y eliminando contenedor..."
# docker rm --force $CONTAINER_NAME
