# simulation-container
Here are all packages referred to the world used in Gazebo

## sim_manager
This package will automatically generate all the necessary files to launch multiple robot simulations.
This is achieved by using `smart-tags` that are located inside the specific launchfile file for each robot, and the configuration within the `toml` file.
It is written in Python3 and by default it generates a simulation with 3 Robots of type `simple_robot`.

### How to create a new simulation

1. Modify the `sim_manger/config/default_simulation_config.toml` configuration file to specify the scenario, the amount of robot, its initial positions and the different sensors.
2. From the local computer terminal running `python3 sim_generator.py` script located in `sim_manager/scripts/`.
3. Execute the simulation inside the container with `roslaunch sim_manager generated_main_{map_name}.launch` where `map_name` is the name specified in the configuration file.

### 1. Setup config file
Modify the `sim_manger/config/default_simulation_config.toml` configuration file to specify:
- scenario
- amount of robot
- its initial positions
- robots sensors

### 2. Create simulation files
Inside the folder `sim_manager/scripts` files can generate running `python3 sim_generator.py`:

```bash
username@host: python3 sim_generator.py
File already exist
Do you want to overwrite it? y/n: y
../launch/generated/boxes2/generated_main_boxes2.launch created
../launch/generated/boxes2/generated_robots_nav.launch created
../config/generated/boxes2/one_robot0_costmap_common_params.yaml created
../config/generated/boxes2/one_robot0_local_costmap_params.yaml created
../config/generated/boxes2/one_robot0_global_costmap_params.yaml created
../config/generated/boxes2/one_robot0_base_local_planner_params.yaml created
../config/generated/boxes2/one_robot1_costmap_common_params.yaml created
../config/generated/boxes2/one_robot1_local_costmap_params.yaml created
../config/generated/boxes2/one_robot1_global_costmap_params.yaml created
../config/generated/boxes2/one_robot1_base_local_planner_params.yaml created
```
These files must be generated from the local computer, since if they were created from the container it would have permissions conflicts.

#### Configuration file
This will generate all the launchfile files from the `toml` configuration file located in the `config` folder.
Here is an example with 3 robots in diferens location.

```yaml
[simulation]
type = "sim description"
map_name = "boxes2"
simulation_type = "nav"
read_initial_pose_from_file = false
robot_count = 1
random_robot_init = 0
robot_initial_pose = "../config/robot_initial_position.txt"

[robot0]
robotid = 0
type = "simple robot"
initial_position = [0.0, 0.0, 0.3, 0.0]
laser_enable = true
rgbd_enable = true

[robot1]
robotid = 1
type = "simple robot"
initial_position = [1.0, 2.0, 0.3, 0.0]
laser_enable = true
rgbd_enable = true

[robot2]
robotid = 2
type = "simple robot"
initial_position = [1.0, 3.0, 0.3, 0.0]
laser_enable = true
rgbd_enable = true

```
The file `default_simulation_config.toml` is a configuration file in a language similar to YAML, in which we have `[labels]` which contain the different elements to be configured:

- [simulation] this is a label required for `sim_manager`
  - type: In this case "SIM description" is to indicate that we are on the configuration label.
  - map_name: indicate the type of scenario we want to create in the simulation.
  - simulation_type: indicate the type of simulation that we want to create [NAV, MAP].
  - read_initial_pose_from_file: indicate whether the initial position of the robots is read on the file indicated below or is read from the `toml` file.
  - robot_count: indicate the amount of robot that we want to instantiate in our simulation (only works when `read_initial_pose_from_file = false`).
  - random_robot_init: indicate if we want the initial position of the robots to be chosen randomly within those indicated by the `robot_initial_pose` file.
  - robot_initial_pose: file with possible initial robot positions.
- [Robot1] label to indicate the configuration of the `robot1`. The name must be unique in the file
  - robotid: robot ID, should be unique in simulation
  - type: type of robot used. By default you can use `simple robot`, and in case of adding the Romaa model you can use `romaa`.
  - initial_position: Position and initial orientation of the robot within the simulation. Position are in meters, orientation in degrees [x, y, z, yaw].
  - laser_enable: option to add to robot a laser sensor.
  - rgbd_enable: option to add to robot an RGB-D camera.


#### Run the simulation
To execute the simulation recently generated we must to run the next command inside the container, where `map_name` is the same specified in `default_simulation_config.toml`

```bash
roslaunch sim_manager generated_main_{map_name}.launch
```

## simple robot description
El robot es del tipo diferencial, y puede ser equipado con diferentes sensores.

In this folder you will find the configuration files of a ready to use robot called `simple_robot`.
It is a differential robot type, and can be equipped with different sensors.


## world package
In this package are located the different scenarios. The `Boxes2` scenario is provided to operate out-off the box.
A map of the scenario is also provided in order to evaluate planning algorithms using `navigation_stack` by ROS.

## How to add new robot models
New robots models needs to be added to `sim-manager/simulation_container/simulation/external` folder.

### Add Husky external robot model
1. Create `sim-manager/simulation_container/simulation/external` folder.
2. Clone Husky repository and its dependencies inside `external/` folder:
```bash
git clone https://github.com/husky/husky.git
git clone https://github.com/clearpathrobotics/LMS1xx.git
git clone https://github.com/lmark1/velodyne_simulator.git
cd husky
git checkout melodic-devel
```
3. Setup husky in `sim_manger/config/default_simulation_config.toml` configuration file to specify the scenario, the amount of robot, its initial positions and the different sensors.
```yaml
[simulation]
type = "sim description"
map_name = "boxes2"
simulation_type = "nav"
read_initial_pose_from_file = false
robot_count = 1
random_robot_init = 0
robot_initial_pose = "../config/robot_initial_position.txt"

[robot0]
robotid = 0
type = "husky"
initial_position = [0.0, 0.0, 0.3, 0.0]
laser_enable = true
rgbd_enable = true
```

