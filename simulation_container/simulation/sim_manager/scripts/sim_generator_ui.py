from tkinter import *
from tkinter.ttk import *
import sys
import os


class RobotConfig():
    def __init__(self, window, row, col, robot_config):
        self.laser_options = ["True", "False"]
        self.rgbd_options = ["True", "False"]


        self.label_id = Label(window, text="robot id:")
        self.label_id.grid(row=row, column=col)
        self.label_id_val = Label(window, text=str(robot_config['robotid']))
        self.label_id_val.grid(row=row, column=col+1)

        self.label_type = Label(window, text="type")
        self.label_type.grid(row=row+1, column=col)

        self.sim_type_entry = Entry(window)
        self.sim_type_entry.grid(row=row+1, column=col+1)
        self.sim_type_entry.insert(0,robot_config['type'])

        self.label_pose = Label(window, text="initial position [x,y,z,yaw]")
        self.label_pose.grid(row=row+2, column=col)
        self.pose_x = Entry(window)
        self.pose_x.grid(row=row+2, column=col+1)
        self.pose_x.insert(0,robot_config['initial_position'][0])
        self.pose_y = Entry(window)
        self.pose_y.grid(row=row+2, column=col+2)
        self.pose_y.insert(0,robot_config['initial_position'][1])
        self.pose_z = Entry(window)
        self.pose_z.grid(row=row+2, column=col+3)
        self.pose_z.insert(0,robot_config['initial_position'][2])
        self.pose_yaw = Entry(window)
        self.pose_yaw.grid(row=row+2, column=col+4)
        self.pose_yaw.insert(0,robot_config['initial_position'][3])

        self.laser_enable = IntVar()
        self.drop_laser = Checkbutton( window, text="laser enable", variable=self.laser_enable,onvalue = 1, offvalue = 0)
        self.drop_laser.grid(row=row+3, column=col)

        self.camera_enable = IntVar()
        self.drop_camera = Checkbutton( window, text="camera enable", variable=self.camera_enable,onvalue = 1, offvalue = 0)
        self.drop_camera.grid(row=row+4, column=col)

    def camera_callback(self,selection):
        self.camera_enable = selection

    def get_robot_id(self):
        return int(self.label_id_val.cget("text"))

    def get_robot_type(self):
        return str(self.sim_type_entry.get())

    def get_robot_initial_pose(self):
        return [float(self.pose_x.get()),
                float(self.pose_y.get()),
                float(self.pose_z.get()),
                float(self.pose_yaw.get())]

    def get_robot_laser(self):
        return self.laser_enable.get()

    def get_robot_camera(self):
        return self.camera_enable.get()



class MainWindow(Frame):
    '''
    main window with menu bar
    '''

    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.grid()

        self.master.title('Main Window')

        menubar = Menu(self.master)
        self.master.config(menu=menubar)


        self._sim_type = "sim description"
        self._sim_name = "boxes2"
        self._map_path = "worlds/"
        self._map_name = "map_boxes2.yaml"
        self.sim_type_list = [" ","nav", "map"]
        self.sim_type_selected = StringVar()
        self._default_config_file_path = "../config/default_simulation_config.toml"

        self._robot_counter = IntVar()
        self._robot_counter_prev = 0
        self._robot_list = []
        self._robot_config_label_id = []
        self._robot_template : {
                'robotid': 0,
                'type' : "simple robot",
                'initial_position' : [0.0, 0.0, 0.0, 0.0],
                'laser_enable' : True,
                'rgbd_enable' : True}

        fileMenu = Menu(menubar)
        fileMenu.add_command(label="Exit", command=self.onExit)
        menubar.add_cascade(label="File", menu=fileMenu)

        editMenu = Menu(menubar)
        menubar.add_cascade(label="Edit", menu=editMenu)

        helpMenu = Menu(menubar)
        helpMenu.add_command(label="About", command="")
        menubar.add_cascade(label="Help", menu=helpMenu)


        self.checkGroup = LabelFrame(self.master, text = "Sim parameters")
        self.checkGroup.grid(row=0, column=0)

        self.sim_cfgfile_label = Label(self.checkGroup, text = "config file path").grid(row=0, column=0)
        self.sim_cfgfile_entry = Entry(self.checkGroup)
        self.sim_cfgfile_entry.grid(row=0, column=1)
        self.sim_cfgfile_entry.insert(0,self._default_config_file_path)

        self.simulation_label = Label(self.checkGroup, text = "[Simulation]").grid(row=1, column=0)

        #  sim_type_label = Label(self.checkGroup, text = "type").grid(row=2, column=0)
        #  self.sim_type_entry = Entry(self.checkGroup, state=DISABLED)
        #  self.sim_type_entry.grid(row=2, column=1)
        #  self.sim_type_entry.insert(0,self._sim_type)

        sim_name_label = Label(self.checkGroup, text = "Sim name").grid(row=3, column=0)
        self.sim_name_entry = Entry(self.checkGroup)
        self.sim_name_entry.grid(row=3, column=1)
        self.sim_name_entry.insert(0, self._sim_name)

        map_path_label = Label(self.checkGroup, text = "Map path").grid(row=4, column=0)
        self.sim_map_path_entry = Entry(self.checkGroup)
        self.sim_map_path_entry.grid(row=4, column=1)
        self.sim_map_path_entry.insert(0, self._map_path)

        map_name_label = Label(self.checkGroup, text = "Map name").grid(row=5, column=0)
        self.sim_map_name_entry = Entry(self.checkGroup)
        self.sim_map_name_entry.grid(row=5, column=1)
        self.sim_map_name_entry.insert(0, self._map_name)


        #  sim_type_label = Label(self.checkGroup, text = "Simulation type").grid(row=5, column=0)
        self.drop = OptionMenu( self.checkGroup , self.sim_type_selected , *self.sim_type_list, command=self.update_selected_sim_type)
        self.drop.grid(row=6, column=1)

        # Robot count
        #  sim_type_label = Label(self.checkGroup, text = "Robot count").grid(row=6, column=0)
        sim_robot_count_entry = Label(self.checkGroup, textvariable=self._robot_counter)
        sim_robot_count_entry.grid(row=7, column=1)
        Button(self.checkGroup, text="-", command=lambda counter=self._robot_counter:  \
                self.decrease_stat(counter=counter)).grid(row=7, column=2)
        Button(self.checkGroup, text="+", command=lambda counter=self._robot_counter:  \
                self.increase_stat(counter=counter)).grid(row=7, column=3)

        # True or False
        self._sim_robot_random_pose = IntVar()
        self.sim_robot_random_pose = Checkbutton(self.checkGroup, text="Robot random initial pose", variable=self._sim_robot_random_pose,onvalue = 1, offvalue = 0)
        self.sim_robot_random_pose.grid(row=8, column=0)

        # True or False
        sim_random_initial_pose_label = Label(self.checkGroup, text = "Robot initial pose file").grid(row=9, column=0)
        self.sim_random_initial_pose_entry = Entry(self.checkGroup, state=DISABLED)
        self.sim_random_initial_pose_entry.grid(row=9, column=1)

        sim_initial_pose_file_label = Label(self.checkGroup, text = "Robot initial pose file path").grid(row=10, column=0)
        self.sim_initial_pose_file_entry = Entry(self.checkGroup, state=DISABLED)
        self.sim_initial_pose_file_entry.grid(row=10, column=1)


        button = Button(self.master, text = "Generate robots" , command = lambda:self.on_click())
        button.grid(row=1,column=0)
        self.rnum = 10

        self.checkGroupRobots = LabelFrame(self.master, text = "Robot config")
        self.checkGroupRobots.grid(row=2, column=0)

    def update_selected_sim_type(self,choice):
        choice = self.sim_type_selected.get()

    def on_click(self):
        print("Generate config")
        self.counters = [IntVar() for _ in range(3)]

        self._robot_list = []
        self._robot_config_label_id = []

        stat = ["Str", "Int", "Dex"]
        for idx in range(0,self._robot_counter.get()):
            self._robot_list.append( {
                'robotid': idx,
                'type' : "simple robot",
                'initial_position' : [float(idx), 0.0, 0.0, 0.0],
                'laser_enable' : True,
                'rgbd_enable' : True})
            self._robot_config_label_id.append(RobotConfig(self.checkGroupRobots, self.rnum, 0, self._robot_list[idx]))
            self.rnum = self.rnum + 5

        self.enable_generate_button()

    def enable_generate_button(self):
        button = Button(self.master, text = "Generate config" , command = lambda:self.generate_config())
        button.grid(row=self.rnum,column=0)
        self.rnum = self.rnum + 1


    def generate_config(self):
        filename = self.sim_cfgfile_entry.get()
        sim_type = "sim description"
        sim_name = self.sim_name_entry.get()
        map_name = self.sim_map_name_entry.get()
        map_path = self.sim_map_path_entry.get()
        simulation_type = self.sim_type_selected.get()
        f = open(filename, "w")

        f.write("\n[simulation]\n")
        f.write("type = \""+str(sim_type)+"\"\n")
        f.write("sim_name = \""+str(sim_name)+"\"\n")
        f.write("map_name = \""+str(map_name)+"\"\n")
        f.write("map_path = \""+str(map_path)+"\"\n")
        f.write("simulation_type = \""+str(simulation_type)+"\"\n")
        f.write("robot_count = "+str(self._robot_counter.get())+"\n")
        f.write("read_initial_pose_from_file = false\n")
        f.write("random_robot_init = 0\n")
        f.write("robot_initial_pose = \""+str(self.sim_initial_pose_file_entry.get())+"\"\n")

        for robot in self._robot_config_label_id:
            robot_id = robot.get_robot_id()
            robot_type = robot.get_robot_type()
            robot_initial_position = robot.get_robot_initial_pose()
            laser_enable = robot.get_robot_laser()
            rgbd_enable = robot.get_robot_camera()
            f.write("\n[robot"+str(robot_id)+"]\n")
            f.write("robotid = "+str(robot_id)+"\n")
            f.write("type = \""+str(robot_type)+"\"\n")
            f.write("initial_position = "+str(robot_initial_position)+"\n")
            f.write("laser_enable = "+str(laser_enable)+"\n")
            f.write("rgbd_enable = "+str(rgbd_enable)+"\n")
        f.close()
        self.print_completed()

    def print_completed(self):
        config_file_done_label = Label(self.master, text = "Config file generated!")
        config_file_done_label.grid(row=self.rnum,column=0)
        self.rnum = self.rnum + 1

        exit_button = Button(self.master, text = "Generate ROS files" , command = lambda:self.generate_ros_simulation_files())
        exit_button.grid(row=self.rnum,column=0)
        self.rnum = self.rnum + 1
        config_file_done_label = Label(self.master, text = "Check the terminal for options")
        config_file_done_label.grid(row=self.rnum,column=0)
        self.rnum = self.rnum + 1

    def generate_ros_simulation_files(self):
        cmd = "python3 sim_generator.py --config ../config/default_simulation_config.toml"
        os.system(cmd)
        exit_button = Button(self.master, text = "Exit" , command = lambda:self.onExit())
        exit_button.grid(row=self.rnum,column=0)

    def onExit(self):
        '''
        on click exit close window and exit
        '''
        self.master.destroy()

    def increase_stat(self,event=None, counter=None):
        counter.set(counter.get() + 1)
        #  self.update_robot_config_list()

    def decrease_stat(self,event=None, counter=None):
        counter.set(counter.get() - 1)
        #  self.update_robot_config_list()

if __name__ == '__main__':
    root = Tk()
    root.state('normal')
    guiFrame = MainWindow(root)
    root.mainloop()
