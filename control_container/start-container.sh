#!/bin/bash
IMAGE_NAME="ros1_control_container:0.1"
CONTAINER_NAME="control-simulation"
CONTROL_ENTRYPOINT_HOST="$(pwd)/entrypoint.sh"
CONTROL_ENTRYPOINT_CONTAINER="/home/entrypoint.sh"
CONTROL_DIR_HOST="$(pwd)/apps"
CONTROL_DIR_CONTAINER="/home/catkin_ws/src/"

list_containers="$(docker ps --format '{{.Names}}' -a)"

echo "Corriendo xhost +local:docker"
xhost +local:docker #Allows GUIs for Docker

echo "Corroborando si existe un contenedor con el mismo nombre"
if ( echo "$list_containers" | fgrep -q $CONTAINER_NAME ); then
    echo " -> Existe un contenedor con el mismo nombre, se procede a eliminarlo"
    docker rm --force $CONTAINER_NAME
else
    echo " -> No existe un contenedor con el mismo nombre"
fi

echo "Procediendo a construir y ejecutar el contenedor"
docker  run -d -i \
        --net=host \
        --volume="/tmp/.X11-unix:/tmp/.X11-unix" \
        --volume="$CONTROL_ENTRYPOINT_HOST:$CONTROL_ENTRYPOINT_CONTAINER" \
        --volume="$CONTROL_DIR_HOST:$CONTROL_DIR_CONTAINER" \
        --name $CONTAINER_NAME \
        $IMAGE_NAME

# --env="DISPLAY=unix$DISPLAY" \

# echo "Ejecutando simulacion CTRL+C para finalizar"
# docker exec -ti $CONTAINER_NAME /bin/bash /home/entrypoint.sh
# echo "Deteniendo y eliminando contenedor..."
# docker rm --force $CONTAINER_NAME
